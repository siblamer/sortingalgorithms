import university.task.sorting.BubbleSort;
import university.task.sorting.InsertionSort;
import university.task.sorting.SelectionSort;

public class Task2 {
    public static void main(String[] args) {
        String[] array = {"Viktor","Andrey"};
        SelectionSort.sort(array);
        BubbleSort.sort(array);
        InsertionSort.sort(array);
        for (String str: array){
            System.out.println(str);
        }
    }
}