import university.task.sorting.BubbleSort;
import university.task.sorting.InsertionSort;
import university.task.sorting.SelectionSort;

public class Task1 {
    public static void main(String[] args) {
        char[] array = new char[] {'A','Z','A','X','h','e','E'};
        SelectionSort.sort(array);
        BubbleSort.sort(array);
        InsertionSort.sort(array);
        for (char ch: array){
            System.out.println(ch);
        }



    }
}